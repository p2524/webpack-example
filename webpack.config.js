const path = require('path');

module.exports = {
  entry: './src/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: require.resolve('./src/add.js'),
        loader: 'expose-loader',
        options: {
          exposes: ["addModule"]
        }
      }
    ]
  },
};