const {add, isValidEmail} = require('./../src/app.js')

describe('Testing app.js', () => {
  it('given 2 and 3 when add should return 5', () => {
    expect(add(2,3)).toBe(5)
  })
  it('given wrong email when isValidEmail should return false', () => {
    expect(isValidEmail('foo')).toBe(false)
  })
})