# Project with example of use of [Webpack][1]

## Launch tests

```
npm test
```

## Launch webpack build

```
npm run build
```

[1]: https://webpack.js.org/