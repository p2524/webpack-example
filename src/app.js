const {add} = require('./add.js')
const validator = require('validator')

const isValidEmail = validator.isEmail

console.log(isValidEmail('asas'))

module.exports = {
  add,
  isValidEmail
}